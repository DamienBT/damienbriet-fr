<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\ProjectRepository;

/**
 * @ORM\Entity(repositoryClass=ProjectRepository::class)
 */
class Project
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\Length(
     *      min = 3,
     *      max = 255,
     *      minMessage = "Le contenu doit contenir au minimum 3 caractères",
     *      maxMessage = "le contenu est trop grand")
     * @ORM\Column(type="string", length=255)
     */
    private $projectName;

    /**
     * @Assert\Length(
     *      min = 3,
     *      max = 255,
     *      minMessage = "Le contenu doit contenir au minimum 3 caractères",
     *      maxMessage = "le contenu est trop grand")
     * @ORM\Column(type="string", length=255)
     */
    private $shortDesc;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=TechnologyList::class, inversedBy="projects")
     */
    private $mainTechnology;

    /**
     * @ORM\Column(type="text")
     */
    private $longDesc;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pictureProject;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnable;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProjectName(): ?string
    {
        return $this->projectName;
    }

    public function setProjectName(string $projectName): self
    {
        $this->projectName = $projectName;

        return $this;
    }

    public function getShortDesc(): ?string
    {
        return $this->shortDesc;
    }

    public function setShortDesc(string $shortDesc): self
    {
        $this->shortDesc = $shortDesc;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getMainTechnology(): ?TechnologyList
    {
        return $this->mainTechnology;
    }

    public function setMainTechnology(?TechnologyList $mainTechnology): self
    {
        $this->mainTechnology = $mainTechnology;

        return $this;
    }

    public function getLongDesc(): ?string
    {
        return $this->longDesc;
    }

    public function setLongDesc(string $longDesc): self
    {
        $this->longDesc = $longDesc;

        return $this;
    }

    public function getPictureProject(): ?string
    {
        return $this->pictureProject;
    }

    public function setPictureProject(string $pictureProject): self
    {
        $this->pictureProject = $pictureProject;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getIsEnable(): ?bool
    {
        return $this->isEnable;
    }

    public function setIsEnable(bool $isEnable): self
    {
        $this->isEnable = $isEnable;

        return $this;
    }
}
