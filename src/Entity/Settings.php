<?php

namespace App\Entity;

use App\Repository\SettingsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SettingsRepository::class)
 */
class Settings
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $introductionText;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $frontSkills;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $backSkills;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $MoreSkills;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIntroductionText(): ?string
    {
        return $this->introductionText;
    }

    public function setIntroductionText(?string $introductionText): self
    {
        $this->introductionText = $introductionText;

        return $this;
    }

    public function getFrontSkills(): ?string
    {
        return $this->frontSkills;
    }

    public function setFrontSkills(?string $frontSkills): self
    {
        $this->frontSkills = $frontSkills;

        return $this;
    }

    public function getBackSkills(): ?string
    {
        return $this->backSkills;
    }

    public function setBackSkills(?string $backSkills): self
    {
        $this->backSkills = $backSkills;

        return $this;
    }

    public function getMoreSkills(): ?string
    {
        return $this->MoreSkills;
    }

    public function setMoreSkills(?string $MoreSkills): self
    {
        $this->MoreSkills = $MoreSkills;

        return $this;
    }
}
