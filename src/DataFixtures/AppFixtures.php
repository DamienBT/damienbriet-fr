<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Project;
use App\Entity\TechnologyList;
use DateTime;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        $technoList = [];


        for ($i = 0; $i < 20; $i++) {
            $technology = new TechnologyList;
            $technology->setName($faker->jobTitle())
                ->setValue(strtolower($technology->getName()))
                ->setIcons($i);
            $manager->persist($technology);
            $technoList[] = $technology;
        }

        for ($p = 0; $p < 5; $p++) {
            $project = new Project;
            $project->setProjectName($faker->catchPhrase())
                ->setShortDesc($faker->sentence())
                ->setMainTechnology($faker->randomElement($technoList))
                ->setCreatedAt(new DateTime());
            $manager->persist($project);
        }

        $manager->flush();
    }
}
