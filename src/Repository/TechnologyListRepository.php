<?php

namespace App\Repository;

use App\Entity\TechnologyList;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TechnologyList|null find($id, $lockMode = null, $lockVersion = null)
 * @method TechnologyList|null findOneBy(array $criteria, array $orderBy = null)
 * @method TechnologyList[]    findAll()
 * @method TechnologyList[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TechnologyListRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TechnologyList::class);
    }

    // /**
    //  * @return TechnologyList[] Returns an array of TechnologyList objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TechnologyList
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
