<?php

namespace App\Controller\Blog;

use App\Entity\Article;
use App\Form\FilterArticleType;
use App\Repository\ArticleRepository;
use App\Service\ArticleViewsService;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BlogController extends AbstractController
{
    /**
     * @Route("/blog", name="blog_index")
     */
    public function index(ArticleRepository $articleRepository, PaginatorInterface $paginator, Request $request)
    {

        $formArticle = $this->createForm(FilterArticleType::class);
        $formArticle->handleRequest($request);
        $bestArticle = $articleRepository->findBy(['id' => $articleRepository->findall()], ['views' => 'DESC'], 10);

        if ($formArticle->isSubmitted() && $formArticle->isValid()) {
            $filter = $formArticle->getData();
            if ($filter['filter'] != null) {
                $articleList = $articleRepository->findBy(['category' => $formArticle->getData('filter'), 'isEnable' => true]);
                if ($this->isGranted('ROLE_USER')) {
                    $articleList = $articleRepository->findBy(['category' => $formArticle->getData('filter')]);
                }
            } else {
                $articleList = $articleRepository->findBy(['isEnable' => true]);
                if ($this->isGranted('ROLE_USER')) {
                    $articleList = $articleRepository->findAll();
                }
            }
            $articles = $paginator->paginate($articleList, $request->query->getInt('page', 1), 6);
        } else {
            $articles = $paginator->paginate($articleRepository->findAll(), $request->query->getInt('page', 1), 6);
        }

        return $this->render('blog/index.html.twig', [
            'articles' => $articles,
            'bestarticles' => $bestArticle,
            'formArticle' => $formArticle->createView(),

        ]);
    }
    /**
     * @Route("article/{slug}", name="article", methods={"GET","POST"})
     */
    public function article(Request $request, Article $article, ArticleViewsService $articleViewsService)
    {
        $articleViewsService->IncrementView($article);

        return $this->render('blog/article.html.twig', [
            'article' => $article,
        ]);
    }
}
