<?php

namespace App\Controller\Administrator;

use DateTime;
use Exception;
use App\Entity\Article;
use App\Form\ArticleType;
use App\Service\FileUploader;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;


/**
 * @Route("/dashboard")
 */
class ArticleController extends AbstractController
{
    /**
     * @Route("/newpost", name="article_new", methods={"GET","POST"})
     */
    public function new(Request $request, FileUploader $fileUploader): Response
    {

        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $article->setDate(new DateTime('now'));
            $article->setViews('0');
            $articleFile = $form['file']->getData();
            $articleTitle = $form['title']->getData();
            $fileUploaded = $fileUploader->upload($articleFile, $articleTitle);

            $entityManager = $this->getDoctrine()->getManager();
            $article->setImageArticle($fileUploaded);
            $article->setIsEnable(true);
            $entityManager->persist($article);
            $entityManager->flush();

            return $this->redirectToRoute('dashboard_index');
        }

        return $this->render('dashboard/article/new.html.twig', [
            'article' => $article,
            'form' => $form->createView(),
        ]);
    }
    /**
     * @Route("/editarticle{id}", name="article_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Article $article): Response
    {
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        $article->setUpdateAt(new DateTime('now'));

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($article);
            $entityManager->flush();

            return $this->redirectToRoute('dashboard_index');
        }

        return $this->render('dashboard/article/edit.html.twig', [
            'article' => $article,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/deletearticle{id}", name="article_delete", methods={"GET", "POST"})
     */
    public function delete(Request $request, Article $article): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($article);
        $entityManager->flush();

        return $this->redirectToRoute('dashboard');
    }

    /**
     * @Route("/resetcount{id}", name="article_resetcount", methods={"GET", "POST"})
     */
    public function resetCount(Request $request, Article $article): Response
    {
        $article->setViews(0);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($article);
        $entityManager->flush();

        return $this->redirectToRoute('dashboard_index');
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("article/active/{id}", name="article_enable", methods={"GET", "POST"})
     */
    public function enable(Request $request, Article $article): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $article->setIsEnable(true);
        $entityManager->flush();

        return $this->redirectToRoute('blog_index');
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("article/desactive/{id}", name="article_disable", methods={"GET", "POST"})
     */
    public function disable(Request $request, Article $article): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $article->setIsEnable(false);
        $entityManager->flush();

        return $this->redirectToRoute('blog_index');
    }
}
