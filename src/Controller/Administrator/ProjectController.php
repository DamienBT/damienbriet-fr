<?php

namespace App\Controller\Administrator;

use DateTime;
use App\Entity\Project;
use App\Form\ProjectType;
use App\Repository\ProjectRepository;
use App\Service\FileUploader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class ProjectController extends AbstractController
{
    /**
     * @Route("/portfolio", name="project_index")
     */
    public function index(ProjectRepository $projectRepository): Response
    {
        return $this->render('portfolio/index.html.twig', [
            'projects' => $projectRepository->findBy([], ['id' => 'DESC']),
        ]);
    }

    /**
     * @Route("dashboard/nouveauprojet", name="project_new", methods={"GET","POST"})
     */
    public function new(Request $request, SluggerInterface $slugger, FileUploader $fileUploader): Response
    {
        $project = new Project();
        $form = $this->createForm(ProjectType::class, $project);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $newproduct = $form->getData();
            $file = $form['pictureProject']->getData();
            $titlefile = $form['projectName']->getData();
            $fileUploaded = $fileUploader->upload($file, $titlefile);
            // $slugger->slug($form->getData());
            $entityManager = $this->getDoctrine()->getManager();
            $project->setCreatedAt(new DateTime());
            $project->setSlug($slugger->slug($newproduct->getProjectName()));
            $project->setPictureProject($fileUploaded);
            $project->setIsEnable(true);
            $entityManager->persist($project);
            $entityManager->flush();

            return $this->redirectToRoute('project_index');
        }

        return $this->render('dashboard/project/new.html.twig', [
            'project' => $project,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("projet/{slug}", name="project_show", methods={"GET"})
     */
    public function show(Project $project): Response
    {
        return $this->render('dashboard/project/show.html.twig', [
            'project' => $project,
        ]);
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("dashboard/{id}/editerprojet", name="project_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Project $project): Response
    {
        $form = $this->createForm(ProjectType::class, $project);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('project_index');
        }

        return $this->render('dashboard/project/edit.html.twig', [
            'project' => $project,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("dashboard/deleteprojet/{id}", name="project_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Project $project): Response
    {
        if ($this->isCsrfTokenValid('delete' . $project->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($project);
            $entityManager->flush();
        }

        return $this->redirectToRoute('project_index');
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("active/{id}", name="project_enable", methods={"GET", "POST"})
     */
    public function enable(Request $request, Project $project): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $project->setIsEnable(true);
        $entityManager->flush();

        return $this->redirectToRoute('project_index');
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("desactive/{id}", name="project_disable", methods={"GET", "POST"})
     */
    public function disable(Request $request, Project $project): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $project->setIsEnable(false);
        $entityManager->flush();

        return $this->redirectToRoute('project_index');
    }
}
