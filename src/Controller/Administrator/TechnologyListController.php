<?php

namespace App\Controller\Administrator;

use App\Entity\TechnologyList;
use App\Form\TechnologyListType;
use App\Repository\TechnologyListRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/dashboard/technologie")
 */
class TechnologyListController extends AbstractController
{
    /**
     * @Route("/", name="technology_list_index", methods={"GET"})
     */
    public function index(TechnologyListRepository $technologyListRepository): Response
    {
        return $this->render('dashboard/technology_list/index.html.twig', [
            'technology_lists' => $technologyListRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="technology_list_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $technologyList = new TechnologyList();
        $form = $this->createForm(TechnologyListType::class, $technologyList);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $technologyList->setIsEnable(true);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($technologyList);
            $entityManager->flush();

            return $this->redirectToRoute('technology_list_index');
        }

        return $this->render('dashboard/technology_list/new.html.twig', [
            'technology_list' => $technologyList,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="technology_list_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, TechnologyList $technologyList): Response
    {
        $form = $this->createForm(TechnologyListType::class, $technologyList);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('technology_list_index');
        }

        return $this->render('dashboard/technology_list/edit.html.twig', [
            'technology_list' => $technologyList,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="technology_list_delete", methods={"GET", "POST"})
     */
    public function delete(Request $request, TechnologyList $technologyList): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($technologyList);
        $entityManager->flush();

        return $this->redirectToRoute('technology_list_index');
    }

    /**
     * @Route("active/{id}", name="technology_list_enable", methods={"GET", "POST"})
     */
    public function enable(Request $request, TechnologyList $technologyList): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $technologyList->setIsEnable(true);
        $entityManager->flush();

        return $this->redirectToRoute('technology_list_index');
    }

    /**
     * @Route("desactive/{id}", name="technology_list_disable", methods={"GET", "POST"})
     */
    public function disable(Request $request, TechnologyList $technologyList): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $technologyList->setIsEnable(false);
        $entityManager->flush();

        return $this->redirectToRoute('technology_list_index');
    }
}
