<?php

namespace App\Controller\Administrator;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DashboardController extends AbstractController
{
    /**
     * @IsGranted("ROLE_USER")
     * @Route("/dashboard", name="dashboard_index")
     */
    public function index(): Response
    {
        return $this->render('dashboard/dashboard_homepage.html.twig', [
            'controller_name' => 'DashboardController',
        ]);
    }
}
