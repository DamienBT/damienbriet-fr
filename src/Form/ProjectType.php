<?php

namespace App\Form;

use App\Entity\Project;
use App\Entity\TechnologyList;
use Symfony\Component\Form\AbstractType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ProjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('projectName', TextType::class, ['attr' => ['class' => 'form-control']])
            ->add('shortDesc', TextType::class, ['attr' => ['class' => 'form-control']])
            ->add('longDesc', CKEditorType::class, ['attr' => ['class' => 'form-control']])
            ->add('pictureProject', FileType::class, ['attr' => ['class' => 'form-control-file'], 'required' => false, 'label' => false, 'mapped' => false, 'constraints' => [
                new File([
                    'maxSize' => '1024k',
                    'mimeTypes' => [
                        'application/pdf',
                        'application/x-pdf',
                        'image/jpeg',
                        'image/png'
                    ],
                    'mimeTypesMessage' => 'Merci d\'envoyer un fichier valide',

                ])
            ]])
            ->add('mainTechnology', EntityType::class, ['class' => TechnologyList::class, 'choice_label' => 'name']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Project::class,
        ]);
    }
}
