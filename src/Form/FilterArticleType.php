<?php

namespace App\Form;

use App\Entity\Category;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FilterArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('filter', EntityType::class, ['class' => Category::class, 'choice_label' => 'libelle', 'label'=> false, 'required' => false, 'attr' => ['class' => 'form-control'], 'placeholder' => 'Tous'])
            ->add('send', SubmitType::class, ['attr' => ['class' => 'btn btn-success'], 'label' => 'Rechercher']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
