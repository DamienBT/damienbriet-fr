<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Article;
use App\Entity\Category;
use Symfony\Component\Form\AbstractType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, ['label' => 'Titre de l\'article', 'attr' => ['class' => 'form-control', 'placeholder' => 'un titre d\'article']])
            ->add('description', TextType::class, ['required' => false, 'label' => 'meta desc', 'attr' => ['class' => 'form-control', 'placeholder' => 'description à renseigner']])
            ->add('keywords', TextType::class, ['required' => false, 'label' => 'Description', 'attr' => ['class' => 'form-control', 'placeholder' => 'Keywords, keywords...']])
            ->add('author', EntityType::class, [
                'class' => User::class,
                'choice_label' => 'lastname',
                'attr' => ['class' => 'form-control']
            ])
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'choice_label' => 'libelle',
                'attr' => ['class' => 'form-control'],
                'required' => false,
                'placeholder' => 'Selectionner une catégorie'
            ])
            ->add('file', FileType::class, ['attr' => ['class' => 'form-control-file'], 'required' => false, 'label' => false, 'mapped' => false, 'constraints' => [
                new File([
                    'maxSize' => '1024k',
                    'mimeTypes' => [
                        'application/pdf',
                        'application/x-pdf',
                        'image/jpeg',
                        'image/png'
                    ],
                    'mimeTypesMessage' => 'Merci d\'envoyer un fichier valide',

                ])
            ]])
            ->add('text', CKEditorType::class, ['label' => 'Contenu', 'attr' => ['class' => 'form-control', 'placeholder' => 'un contenu clair et précis', 'rows' => '20']]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
