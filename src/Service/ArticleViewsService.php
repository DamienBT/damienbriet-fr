<?php

namespace App\Service;

use App\Entity\Article;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class ArticleViewsService
{
    private $manager;
    private $security;

    public function __construct(EntityManagerInterface $manager, Security $security)
    {
        $this->manager = $manager;
        $this->security = $security;
    }

    public function IncrementView(Article $article)
    {

        $count = $article->getViews();
        if ($this->security->getUser() == null || $this->security->getUser() == '') {
            $count = $count + 1;
        };
        $article->setViews($count);
        $this->manager->persist($article);
        $this->manager->flush();
    }
}
