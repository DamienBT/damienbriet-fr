<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210602095943 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE article ADD is_enable TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE category ADD is_enable TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE project ADD is_enable TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE technology_list ADD is_enable TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE article DROP is_enable');
        $this->addSql('ALTER TABLE category DROP is_enable');
        $this->addSql('ALTER TABLE project DROP is_enable');
        $this->addSql('ALTER TABLE technology_list DROP is_enable');
    }
}
