<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210218144051 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE project ADD main_technology_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EEF09167FE FOREIGN KEY (main_technology_id) REFERENCES technology_list (id)');
        $this->addSql('CREATE INDEX IDX_2FB3D0EEF09167FE ON project (main_technology_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EEF09167FE');
        $this->addSql('DROP INDEX IDX_2FB3D0EEF09167FE ON project');
        $this->addSql('ALTER TABLE project DROP main_technology_id');
    }
}
